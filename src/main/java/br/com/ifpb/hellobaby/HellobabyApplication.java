package br.com.ifpb.hellobaby;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HellobabyApplication {

	public static void main(String[] args) {
		SpringApplication.run(HellobabyApplication.class, args);
	}

}
